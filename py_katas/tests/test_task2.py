def matching_sets(a, b):
    res = set()
    for i in a:
        for z in b:
            if i == z:
                res.add(i)
    return res
                        




def test_matching_sets():
    a = {2, 1, 4, 8, 120}
    b = {3, 2, 1, 7, 120}
    
    
    assert matching_sets(a,b) == {1, 2, 120}
    assert matching_sets({1, 2, 3}, {1, 4, 7}) == {1}
    assert matching_sets({1, 2, 3}, {9, 4, 7}) == set()
    assert matching_sets({1}, {7}) == set()
    assert matching_sets({}, {}) == set() 
    