def primes_In_Range(x):
    prime_list = []
    for num in range(1, x + 1):
        if num > 1:
            for i in range(2, num):
                if (num % i) == 0:
                                break
            else:
                prime_list.append(num)
    return prime_list


def test_primes_In_Range():
    
    assert primes_In_Range(1) == []
    assert primes_In_Range(7)== [2,3,5,7]