import requests
import csv


response_usr = requests.get("http://jsonplaceholder.typicode.com/users")
response_posts = requests.get("http://jsonplaceholder.typicode.com/posts")
users = response_usr.json()
users_posts = response_posts.json()



for user in users:
    for post in users_posts:
        if user['id'] == post['userId']: 
            if 'linked_posts' in user.keys():
                user['linked_posts'].append(post)
            elif 'linked_posts' not in user.keys():
                user['linked_posts'] = [post]



with open('my_file.csv', mode='w') as file:
    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(('userId','user_name','post_id','post_title', 'body'))
    for user in reversed(users):
        user_id = user['id']
        user_name = user['name']
        for post in user['linked_posts']:
            post_id = post['id']
            post_title = post['title']
            if post['body'].find('nobis'):
                post_body = 'I have nobis'
            else:
                post_body = 'I do not have nobis'
            row = (f'{user_id}, {user_name}, {post_id}, {post_title}, {post_body}')
            writer.writerow([row])
            